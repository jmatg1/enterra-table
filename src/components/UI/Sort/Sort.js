import React from 'react';
import PropTypes from 'prop-types';
import './Sort.less'

const MyComponent = props => {
  let styleAsc = ["arrows__item", "arrows__up"]
  let styleDesc = ["arrows__item", "arrows__down"]
  switch (props.sort) {
    case 'asc':
      styleAsc.push("active")
      break;
    case 'desc':
      styleDesc.push("active")
      break;
    default: break;
  }
  return (
    <div className="arrows">
      <div onClick={props.sortAsc} className={styleAsc.join(" ")}></div>
      <div onClick={props.sortDesc} className={styleDesc.join(" ")}></div>
    </div>
  );
};

MyComponent.propTypes = {
  sortAsc: PropTypes.func.isRequired,
  sortDesc: PropTypes.func.isRequired,
  sort: PropTypes.string.isRequired,
};

export default MyComponent;
