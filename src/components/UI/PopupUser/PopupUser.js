import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './PopupUser.less'
import Button from '../Button/Button'
import Rating from '../../Rating/Rating'
class PopupUser extends Component {
  render() {
    // Render nothing if the "show" prop is false
    if(!this.props.show) {
      return null;
    }

    return (
      <div className="backdrop">
        <div className="modal" >
          {this.props.children}
          <div className="modal__body">
            <div className="modal__body__row">
              <div className="modal__body__status">{this.props.user.online ? "Онлайн" : "Нет в сети" }</div>
            </div>
            <div className="modal__body__row">
              <div className="modal__body__name" title={this.props.user.name}>
                {this.props.user.name}
              </div>
              <Rating size="2x" total={5}/>
            </div>
          </div>
          <div className="modal__footer">
            <Button clicked={this.props.onClose}>
              ЗАКРЫТЬ
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

PopupUser.propTypes = {
  onClose: PropTypes.func.isRequired,
  show: PropTypes.bool,
  children: PropTypes.node
};

export default PopupUser;