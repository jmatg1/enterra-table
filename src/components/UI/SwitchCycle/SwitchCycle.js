import React from 'react';
import PropTypes from 'prop-types';
import './SwitchCycle.less'
const switchCycle = props => {
  let style = ["cycle"];
  if (props.status)
    style.push("on")
  else
    style.push("off")
  return (
    <div className={style.join(" ")}></div>
  );
};

switchCycle.propTypes = {
  status: PropTypes.bool.isRequired,
};

export default switchCycle;
