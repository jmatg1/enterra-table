import React from 'react';
import PropTypes from 'prop-types';
import './ContextMenu.less'
const ContextMenu = props => {
  const style = {
    display: 'block',
    top: props.y,
    left: props.x
  }
  if (!props.show)
    style.display = 'none'

  const children = props.children;

  return (
    <div className="context-menu-bk" style={style}  >
      <span onClick={props.onClose} role="img" aria-label="cross" className="context-menu-bk__close">&#10060;</span>
      <ul  ref={props.elRef} className="context-menu-bk__list">
        {children}
      </ul>
    </div>
  );
};

ContextMenu.propTypes = {
  children: PropTypes.node
};

export default ContextMenu;
