import React from 'react';
import PropTypes from 'prop-types';

const contextItem = props => {
  return (
    <>
  <li onClick={props.clicked}>{props.children}  <div className="line"></div></li>

      </>

  );
};

contextItem.propTypes = {
  children: PropTypes.string,
  clicked: PropTypes.func,
};

export default contextItem;
