import React from 'react';
import PropTypes from 'prop-types';

const tableRow = props => {
  const children = props.children;
  let style = {};
  if (props.hide)
    style = {display:"none"}
  return (
    <tr style={style}>
      {children}
    </tr>
  );
};

tableRow.propTypes = {
  children: PropTypes.element.isRequired
};

export default tableRow;
