import React from 'react';
import PropTypes from 'prop-types';
import './Table.less'
const table = props => {
  const children = props.children
  return (
    <table className="fixed_headers">
      {children}
    </table>
  );
};

table.propTypes = {
  children: PropTypes.element.isRequired
};

export default table;