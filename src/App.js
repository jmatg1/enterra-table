import React, { Component } from 'react';
import './App.less';
import playersData from './data/players.json';
import Table from './components/Table/Table'
import TableHead from './components/Table/TableHead/TableHead'
import TableBody from './components/Table/TableBody/TableBody'
import TableRow from './components/Table/TableRow/TableRow'
import TableCell from './components/Table/TableCell/TableCell'
import Star from './components/Rating/Rating'
import SwitchCycle from './components/UI/SwitchCycle/SwitchCycle'
import Button from './components/UI/Button/Button'
import Sort from './components/UI/Sort/Sort'
import PopupUser from './components/UI/PopupUser/PopupUser'
import ContextMenu from './components/UI/ContextMenu/ContextMenu'
import ContextItem from './components/UI/ContextMenu/ContextItem/ContextItem'

class App extends Component {

  state = {
    players: [],
    sortPlayers: [],
    userPopup: {
      show: false,  // Юзер в попапе
      user: null
    },
    contextMenu: {
      show: false,
      idUser: null,
      y: 0,
      x: 0
    },
    userName: '',   // имя юзера дл поиска
    userOnline: false,  //online or offline
    tableHead:  {
      list: [
        {
          id: 'id',
          name: 'ID',
          sort: ''   // asc or desc
        },
        {
          id: 'name',
          name: 'Имя',
          sort: ''
        },
        {
          id: 'level',
          name: 'Уровень',
          sort: ''
        },
        {
          id: 'online',
          name: 'Онлайн',
          sort: ''
        }
      ],
      lastSort: {
        index: null,
        sort: ''
      } // где последний раз была сортировка
    },
  };

  componentDidMount() {
    const playersDataArray = JSON.parse(JSON.stringify(playersData))

    const players = playersDataArray.map(item => {
      let copy = {...item};
      copy.visible = true;            // скрывает пользователь
      copy.sortShow = true;           // скрывает сортировка
      return copy
    })
    const players2 = playersDataArray.map(item => {
      let copy2 = {...item};
      copy2.visible = true;            // скрывает пользователь
      copy2.sortShow = true;           // скрывает сортировка
      return copy2
    })
    this.setState({players: players, sortPlayers: players2})
  }
  // Сортировка
  sortTable = () => {
    const regex = new RegExp(this.state.userName);
    let sortPlayers = []
    const oldPlayers = [...this.state.players]
    sortPlayers = oldPlayers.map((el,i) => {
      if (regex.test(el.name) && el.online === this.state.userOnline){
        const newEl = {...el}
        newEl.sortShow = true;
        return (newEl)
      }
      else{
        const newEl = {...el}
        newEl.sortShow = false;
        return (newEl)
      }
    })
    this.setState({ players: sortPlayers })
  }

  changeCheckboxHandler = () => {
    this.setState({
      userOnline:  !this.state.userOnline
    }, () => {
      this.sortTable();
    });
  }

  changeNameHandler = (event) => {
    this.setState({
      userName:  event.target.value
    }, () => {
      this.sortTable();
    });
  }

  showAllHandler = () => {
    let oldTableHead =  {...this.state.tableHead};
    const lastSortIndex = oldTableHead.lastSort.index;

    if(lastSortIndex) {
      oldTableHead.list[lastSortIndex].sort = '';
      oldTableHead.lastSort.index = null;
    }

    this.setState({userName:'', userOnline: false, players: this.state.sortPlayers, tableHead: oldTableHead})
  }

  compareByAsc(key) {
    return function (a, b) {
      if (a[key] < b[key]) return -1;
      if (a[key] > b[key]) return 1;
      return 0;
    };
  }

  compareByDesc(key) {
    return function (a, b) {
      if (a[key] > b[key]) return -1;
      if (a[key] < b[key]) return 1;
      return 0;
    };
  }

  sortBy(key,by) {
    let arrayCopy = [...this.state.players];
    if (by === "asc")
      arrayCopy.sort(this.compareByAsc(key));
    else
      arrayCopy.sort(this.compareByDesc(key));
    this.setState({players: arrayCopy});
  }

  sortHandler = (index, by) => {
    let oldTableHead =  {...this.state.tableHead};
    const key = oldTableHead.list[index].id;
    const lastSortIndex = oldTableHead.lastSort.index;

    if(lastSortIndex === null){
      oldTableHead.list[index].sort = by;
      oldTableHead.lastSort.index = index;
    }else {
      oldTableHead.list[lastSortIndex].sort = '';
      oldTableHead.list[index].sort = by;
      oldTableHead.lastSort.index = index;
    }

    this.setState({tableHead: oldTableHead})
    this.sortBy(key,by)
  }
  // Сортировка END
  closePopupUser = () => {
    let old = {...this.state.userPopup};
    old.show = false
    this.setState({userPopup:old})
  }
  contextMenuHandler = (id,ev) => {

    const heightRow = ev.target.offsetHeight;                 // высота строки
    const popupOffsetY = ev.target.getBoundingClientRect().y; // расстояние до строки
    const popupOffsetX = ev.target.getBoundingClientRect().x;
    const scrollOffset = window.pageYOffset;                  // положение скрола
    const windowHeight = window.innerHeight;                  // высота всего экрана
    const heightPopup  = this._popupEl.offsetHeight;          // высота попапа
    const widthPopup  = this._popupEl.offsetWidth;

    let popupOffset = heightRow + popupOffsetY + scrollOffset;

    if ( popupOffset + heightPopup > windowHeight + scrollOffset )
      popupOffset = popupOffset - heightPopup - heightRow

    let old = {...this.state.contextMenu}
    old.show = true;
    old.idUser = id;
    old.y = popupOffset
    old.x = popupOffsetX - widthPopup / 3
    this.setState({contextMenu: old})

  }
  closeContextMenu = () => {
    let updContextMenu = {...this.state.contextMenu}
    updContextMenu.show = false;

    this.setState({contextMenu: updContextMenu})
  }
  openProfileHandler = () => {
    const oldPopup = {...this.state.userPopup}
    oldPopup.show = true;

    const oldContextMenu = {...this.state.contextMenu}
    oldContextMenu.show = false;

    this.setState({userPopup: oldPopup, contextMenu: oldContextMenu})
  }
  hideUserHandler = () => {

    const oldContextMenu = {...this.state.contextMenu}
    oldContextMenu.show = false;

    const id = this.state.contextMenu.idUser;

    let updPl = [...this.state.players];
    let item = {...updPl[id]};
    item.visible = false;
    updPl[id] = item;

    this.setState({
        players: updPl,
        contextMenu: oldContextMenu
    })

  }
  render() {
    const itemHead = this.state.tableHead.list.map((item,i) => {
      return (
        <TableCell key={i} type="th" sort={item.sort}>
          <div className="th-sort">
            <div className="th-sort__name">{item.name}</div>

            <Sort
              sort={item.sort}
              sortAsc={() => this.sortHandler(i,"asc")}
              sortDesc={()=> this.sortHandler(i,"desc")}
            />
          </div>
        </TableCell>
      )
    });
    const itemBody = this.state.players.map(item => {
      return (
        <TableRow key={item.id} hide={!item.sortShow || !item.visible}>
          <>
            <TableCell type="td">{item.id + 1}</TableCell>
            <TableCell type="td"><div className="player-name" onClick={(ev) => this.contextMenuHandler(item.id,ev)}>{item.name}</div></TableCell>
            <TableCell type="td"><Star total={item.level} size="x1"/></TableCell>
            <TableCell type="td"><SwitchCycle status={item.online}/></TableCell>
          </>
        </TableRow>
      )
    });

    return (
      <div className="App" >
        <PopupUser
          onClose={this.closePopupUser}
          user={this.state.players[this.state.contextMenu.idUser]}
          show={this.state.userPopup.show}
        />
        <ContextMenu
          elRef={(node) => { this._popupEl = node}}
          onClose={this.closeContextMenu}
          show={this.state.contextMenu.show}
          y={this.state.contextMenu.y}
          x={this.state.contextMenu.x}>
          <ContextItem clicked={this.openProfileHandler}>Показать профиль</ContextItem>
          <ContextItem clicked={this.hideUserHandler}>Скрыть игрока</ContextItem>
        </ContextMenu>
        <div className="table-cnl">
          <label className="table-cnl__label">
            Имя
            <input
              onChange={this.changeNameHandler}
              value={this.state.userName}
              className="table-cnl__name"
              type="text"/>
          </label>
          <label className="table-cnl__label">
            Онлайн
            <input
              onChange={this.changeCheckboxHandler}
              checked={this.state.userOnline}
              className="table-cnl__checkbox"
              type="checkbox"/>
          </label>
          <Button clicked={this.showAllHandler}>Показать всех</Button>
        </div>
       <Table>
         <>
          <TableHead>
            <TableRow>
              <>
              {itemHead}
              </>
            </TableRow>
          </TableHead>
           <TableBody>
             <>
               {itemBody}
             </>
           </TableBody>
         </>
       </Table>
      </div>
    );
  }
}

export default App;
